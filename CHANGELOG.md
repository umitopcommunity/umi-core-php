# Changelog

## [1.0.8][] - 2022-07-20
### Added
- Поддержка tx version - 8
- Обновлена генерация SecretKey

## [1.0.71][] - 2020-06-14

### Added

- Классы `SecretKey`, `PublicKey`, `Address`, `Transaction`

[1.0.8]: https://gitlab.com/umitopcommunity/umi-core-php/-/tags/1.0.8
[1.0.71]: https://gitlab.com/umitopcommunity/umi-core-php/-/tags/1.0.7